const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER,varsta VARCHAR(3),cnp VARCHAR(20), dataNastere VARCHAR(6))";
    connection.query(sql, function (err, result) {
        if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta = req.body.varsta;
    let cnp = req.body.cnp;
    let sex = req.body.sex;
    let dataNastere = req.body.dataNastere;
    let error = [];

    if (!nume||!prenume||!telefon||!email||!facebook||!tipAbonament||!nrCard||!cvv||!varsta||!cnp||!dataNastere) {
        error.push("Unul sau mai multe campuri nu au fost introduse");
        console.log("Unul sau mai multe campuri nu au fost introduse!");
      } else {
        if (nume.length < 3 || nume.length > 20) {
          console.log("Nume invalid!");
          error.push("Nume invalid");
        } else if (!nume.match("^[A-Za-z]+$")) {
          console.log("Numele trebuie sa contina doar litere!");
          error.push("Numele trebuie sa contina doar litere!");
        }
        if (prenume.length < 3 || prenume.length > 20) {
          console.log("Prenume invalid!");
          error.push("Prenume invalid!");
        } else if (!prenume.match("^[A-Za-z]+$")) {
          console.log("Prenumele trebuie sa contina doar litere!");
          error.push("Prenumele trebuie sa contina doar litere!");
        }
        if (telefon.length != 10) {
          console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
          error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
        } else if (!telefon.match("^[0-9]+$")) {
          console.log("Numarul de telefon trebuie sa contina doar cifre!");
          error.push("Numarul de telefon trebuie sa contina doar cifre!");
        }
        if (!email.includes("@gmail.com") && !email.includes("@yahoo.com")) {
          console.log("Email invalid!");
          error.push("Email invalid!");
        }
        if (!facebook.includes("https://www.facebook.com/")) {
            console.log("Link de Facebook invalid!");
            error.push("Link de Facebook invalid!");
        }
        if (tipAbonament != "Basic" || tipAbonament != "Standard" || tipAbonament != "Premium") {
            console.log("Tipul abonamentului este invalid");
            error.push("Tipul abonamentului este invalid");
        }
        if (nrCard.length != 16) {
            console.log("Numarul cardului trebuie sa fie de 16 cifre!");
            error.push("Numarul cardului trebuie sa fie de 16 cifre!");
        } else if (nrcard.match("^[0-9]+$")) {
            console.log("Numarul cardului trebuie sa contina doar cifre!");
            error.push("Numarul cardului trebuie sa contina doar cifre!");
        }
        if (cvv.length != 3) {
            console.log("Numarul CVV trebuie sa fie de 3 cifre!");
            error.push("Numarul CVV trebuie sa fie de 3 cifre!")
        } else if (cvv.match("^[0-9]+$")) {
            console.log("Numarul CVV trebuie sa contina doar cifre");
            error.push("Numarul CVV trebuie sa contina doar cifre");
        }
        if (varsta.length <1 || varsta.length>3) {
            console.log("Varsta invalida!");
            error.push("Varsta invalida");
        } else if (varsta.match("^[0-9]+$")) {
            console.log("Varsta trebuie sa contina doar cifre!");
            error.push("Varsta trebuie sa contina doar cifre!");
        }
        if (cnp.length != 13) {
            console.log("CNP invalid!");
            error.push("CNP invalid!");
        } else if (cnp.match("^[0-9]+$")) {
            console.log("CNP-ul trebuie sa contina doar cifre!");
            error.push("CNP-ul trebuie sa contina doar cifra");
        }
        // data de nastere trebuie pusa in format AALLZZ :) 
        if (dataNastere !=6) {
            console.log("Data de nastere invalida!");
            error.push("Data de nastere invalida");
        } else if (dataNastere.match("^[0-9]+$")) {
            console.log("Data de nastere trebuie sa contina doar cifre!");
            error.push("Data de nastere trebuie sa contina doar cifre!")
        }
      }
      //si acum cu o tehnica incredibil de avansata verificam daca data de nastere si cnp-ul sunt compatibile  
        if (dataNastere[0]!=cnp[1]||dataNastere[1]!=cnp[2]||dataNastere[2]!=cnp[3]||dataNastere[3]!=cnp[4]||dataNastere[4]!=cnp[5]||dataNastere[5]!=cnp[6]){
            console.log("Data de nastere si CNP-ul nu sunt compatibile!");
            error.push("Data de nastere si CNP-ul nu sunt compatibile");
        }

        //seeecs
        if (cnp[0]===1 || cnp[0]===3 || cnp[0]===5){
            sex="M";
        } else if (cnp[0]===2 || cnp[0]===4 || cnp[0]===6) {
            sex="F";
        }

    if (error.length === 0) {
        const sql =
            "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv,varsta,cnp,sex,dataNastere) VALUES('" +nume +"','" +prenume +"','" +telefon +"','" +email +"','" +facebook +"','" +tipAbonament +"','" +nrCard +"','" +cvv +"','" +varsta+"','" +cnp+"','"+sex+"','"+dataNastere+ "')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    }
});